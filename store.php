<?php

session_start();

if (strtoupper($_SERVER['REQUEST_METHOD'] == "POST")) {

    if (array_key_exists('description', $_POST) && !empty($_POST['description'])) {

        $_SESSION['formdata'][$_POST['id']] = $_POST;

        header('location:index.php');

    } else {

        header('location:create.php');

    }

} else {

    header('location:create.php');

}